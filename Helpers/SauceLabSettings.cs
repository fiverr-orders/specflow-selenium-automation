namespace SpecFlowAutomationSuite.Helpers
{
    public class SauceLabSettings
    {
        public string Uri { get; set; }
        public string Username { get; set; }
        public string AccessKey { get; set; }
    }
}