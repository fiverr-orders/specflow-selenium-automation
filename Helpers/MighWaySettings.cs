namespace SpecFlowAutomationSuite.Helpers
{
    public class MighWaySettings
    {
        public string Browser { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool Ssl { get; set; }
        public int Timeout { get; set; }
        public int RetryInterval { get; set; }
        public bool ConsiderInvisibleElements { get; set; }
        
        public string RentalPagePath { get; set; }
    }
}