using System;
using TechTalk.SpecFlow;

namespace SpecFlowAutomationSuite.Helpers
{
    [Binding]
    public sealed class Hooks
    {
        // For additional details on SpecFlow hooks see http://go.specflow.org/doc-hooks
        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            // throw new NotImplementedException("BeforeTestRun is not implemented yet!");
        }

        [BeforeFeature]
        public static void FeatureSetup()
        {
            // throw new NotImplementedException("FeatureSetup is not implemented yet!");
        }

        [BeforeScenario]
        public static void ScenarioSetUp()
        {
            // throw new NotImplementedException("ScenarioUp is not implemented yet!");
        }


        [AfterScenario]
        public static void ScenarioTearDown()
        {
            // throw new NotImplementedException("ScenarioTearDown is not implemented yet!");
        }

        [AfterFeature]
        public static void AfterFeature(FeatureContext featureContext)
        {
            // throw new NotImplementedException("AfterFeature is not implemented yet!");
        }
    }
}