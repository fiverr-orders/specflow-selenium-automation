using System;
using Coypu;
using Coypu.Drivers;
using NUnit.Framework.Internal;
using OpenQA.Selenium;
using SpecFlowAutomationSuite.Drivers;

namespace SpecFlowAutomationSuite.Helpers
{
    public static class BrowserFactory
    {
        public static BrowserSession GetBrowser(TestSettings settings)
        {
            var configuration = GetSessionConfiguration(settings);

            if (TestSettings.IsTesting)
            {
                configuration.Driver = typeof(SauceLabsWebDriver);    
            }
            
            return new BrowserSession(configuration);
        }
        
        private static SessionConfiguration GetSessionConfiguration(TestSettings settings)
        {
            return new SessionConfiguration
            {
                Browser = Browser.Parse(settings.MighWay.Browser),
                AppHost = settings.MighWay.Host,
                Port = settings.MighWay.Port,
                SSL = settings.MighWay.Ssl,
                Timeout = TimeSpan.FromSeconds(settings.MighWay.Timeout),
                RetryInterval = TimeSpan.FromSeconds(settings.MighWay.RetryInterval),
                ConsiderInvisibleElements = settings.MighWay.ConsiderInvisibleElements
            };
        }
    }
}