using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace SpecFlowAutomationSuite.Helpers
{
    public class TestSettings
    {
        public MighWaySettings MighWay { get; }
        public SauceLabSettings SauceLab { get; }

        public TestSettings()
        {
            var configurationRoot = GetConfiguration(Path.Combine(AppContext.BaseDirectory));
         
            IServiceCollection services = new ServiceCollection();

            services.AddOptions();
            services.Configure<MighWaySettings>(configurationRoot.GetSection(nameof(MighWaySettings)));
            services.Configure<SauceLabSettings>(configurationRoot.GetSection(nameof(SauceLabSettings)));

            var serviceProvider = services.BuildServiceProvider();

            this.MighWay = serviceProvider.GetRequiredService<IOptions<MighWaySettings>>().Value;
            this.SauceLab = serviceProvider.GetRequiredService<IOptions<SauceLabSettings>>().Value;
        }

        private static IConfigurationRoot GetConfiguration(string outputPath)
        {
            Console.WriteLine($"Current environment: {CurrentEnvironment}");
            
            return new ConfigurationBuilder()
                .SetBasePath(outputPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{CurrentEnvironment}.json")
                .AddEnvironmentVariables()
                .Build();
        }

        public static bool IsTesting => CurrentEnvironment == "testing";

        private static string CurrentEnvironment => Environment.GetEnvironmentVariable("DOTNETCORE_ENVIRONMENT") ?? "development";
    }
}