using System;
using Coypu.Drivers;
using Coypu.Drivers.Selenium;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using SpecFlowAutomationSuite.Helpers;

namespace SpecFlowAutomationSuite.Drivers
{
    public class SauceLabsWebDriver : SeleniumWebDriver
    {
        public SauceLabsWebDriver(Browser browser) : this(CreateSauceLabWebDriver(), browser)
        {
        }

        private SauceLabsWebDriver(IWebDriver webDriver, Browser browser) : base(webDriver, browser)
        {
        }

        private static IWebDriver CreateSauceLabWebDriver()
        {
            var settings = new TestSettings();
            var capabilities = new DesiredCapabilities();
            
            capabilities.SetCapability("browserName", "Chrome");
            capabilities.SetCapability("platformName", "macOS 10.15");
            // capabilities.SetCapability("username", settings.SauceLab.Username);
            capabilities.SetCapability("accessKey", settings.SauceLab.AccessKey);

            IWebDriver driver = new RemoteWebDriver(
                new Uri(settings.SauceLab.Uri),
                capabilities,
                TimeSpan.FromSeconds(600));

            return driver;
        }
    }
}