﻿using System;
using NUnit.Framework;
using SpecFlowAutomationSuite.PageObjects;
using SpecFlowAutomationSuite.ScenarioContext;
using TechTalk.SpecFlow;

namespace SpecFlowAutomationSuite.Steps
{
    [Binding]
    public class SearchPageSteps
    {
        private readonly SearchPage _searchPage;
        private readonly SearchPageContext _context;

        public SearchPageSteps(SearchPage searchPage, SearchPageContext context)
        {
            this._searchPage = searchPage;
            this._context = context;
        }

        [Given(@"I am on the search page refine panel with Auckland and (.*) berths filter")]
        public void GivenIAmOnTheSearchPageRefinePanelWithAucklandAndBerthsFilter(int berths)
        {
            const int birthCount = 9;
            const string location = "Auckland, New Zealand";

            this._searchPage.NavigateToRental();
            this._searchPage.FindElements();
            this._searchPage.SelectBirths(birthCount);

            Assert.AreEqual(this._searchPage.GetLocation(), location);
            Assert.AreEqual(this._searchPage.GetSelectedBirths(), $"{birthCount} Berths");
            Assert.IsTrue(this._searchPage.RefineButtonExists());

            this._searchPage.ClickRefineButton();
        }

        [When(@"I select Instant book checkbox")]
        public void WhenISelectInstantBookCheckbox()
        {
            Assert.IsTrue(this._searchPage.InstantBookingTextExists());
            this._searchPage.CheckInstantBook();
        }

        [When(@"I select Motorhome checkbox")]
        public void WhenISelectMotorHomeCheckbox()
        {
            Assert.IsTrue(this._searchPage.MotorHomeExists());
            this._searchPage.CheckMotorHome();
        }

        [When(@"I unselect Toilet and Kitchen filter")]
        public void WhenIUnselectToiletAndKitchenFilter()
        {
            Assert.IsTrue(this._searchPage.ToiletCheckboxExists());
            Assert.IsTrue(this._searchPage.KitchenCheckboxExists());

            this._searchPage.UncheckToilet();
            this._searchPage.UncheckKitchen();
        }

        [When(@"I select Shower filter")]
        public void WhenISelectShowerFilter()
        {
            Assert.IsTrue(this._searchPage.ShowerCheckboxExists());
            this._searchPage.CheckShower();
        }

        [When(@"I check Auto and Manual Transmission filter")]
        public void WhenICheckAutoAndManualTransmissionFilter()
        {
            Assert.IsTrue(this._searchPage.TransmissionAutoExists());
            Assert.IsTrue(this._searchPage.TransmissionManualExists());

            this._searchPage.CheckBothTransmissions();
        }

        [When(@"I unselect Toilet and Shower filter")]
        public void WhenIUnselectToiletAndShowerFilter()
        {
            Assert.IsTrue(this._searchPage.ToiletCheckboxExists());
            Assert.IsTrue(this._searchPage.ShowerCheckboxExists());

            this._searchPage.UncheckToilet();
            this._searchPage.UncheckShower();
        }

        [When(@"I select Kitchen filter")]
        public void WhenISelectKitchenFilter()
        {
            Assert.IsTrue(this._searchPage.KitchenCheckboxExists());
            this._searchPage.UncheckKitchen();
        }

        [When(@"I select Campervan checkbox")]
        public void WhenISelectCamperVanCheckbox()
        {
            Assert.IsTrue(this._searchPage.CamperVanExists());

            this._searchPage.CheckCamperVan();
        }

        [When(@"I select Toilet filter")]
        public void WhenISelectToiletFilter()
        {
            Assert.IsTrue(this._searchPage.ToiletCheckboxExists());

            this._searchPage.UncheckToilet();
        }

        [When(@"I select Caravan checkbox")]
        public void WhenISelectCaravanCheckbox()
        {
            Assert.IsTrue(this._searchPage.CaravanExists());

            this._searchPage.CheckCaravan();
        }

        [When(@"I uncheck Auto and Manual Transmission filter")]
        public void WhenIUncheckAutoAndManualTransmissionFilter()
        {
            Assert.IsTrue(this._searchPage.TransmissionAutoExists());
            Assert.IsTrue(this._searchPage.TransmissionManualExists());

            this._searchPage.UncheckBothTransmissions();
        }

        [When(@"I select Fifthwheeler checkbox")]
        public void WhenISelectFifthWheelerCheckbox()
        {
            Assert.IsTrue(this._searchPage.FifthWheelExists());

            this._searchPage.CheckFifthWheeler();
        }

        [When(@"I select Heavy Vehicle checkbox")]
        public void WhenISelectHeavyVehicleCheckbox()
        {
            Assert.IsTrue(this._searchPage.BusExists());

            this._searchPage.CheckBus();
        }

        [Then(@"I should be not see result search card")]
        public void ThenIShouldBeNotSeeResultSearchCard()
        {
            Assert.IsTrue(this._searchPage.ResultSearchCardDoesNotExist());
        }

        [Then(@"I should be able to see result search card for (.*)")]
        public void ThenIShouldBeAbleToSeeResultSearchCard(string cardName)
        {
            Assert.IsTrue(this._searchPage.ResultSearchCardExists(cardName));
        }

        [Then(@"I should be able to see (.*) results message")]
        public void ThenIShouldBeAbleToSeeResultsMessage(int searchResultCount)
        {
            Assert.IsTrue(this._searchPage.LocationMessageExists());
            Assert.IsTrue(this._searchPage.FilterMessageExists());
            Assert.IsTrue(this._searchPage.TravelDatesMessageExists());
        }
    }
}