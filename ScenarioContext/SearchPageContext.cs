namespace SpecFlowAutomationSuite.ScenarioContext
{
    public class SearchPageContext
    {
        public string PageTitle => "Mighway | Motorhome Rental - Plan, Book & Explore";
    }
}