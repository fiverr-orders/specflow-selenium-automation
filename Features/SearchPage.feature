﻿Feature: SearchPage
	In order to test the login functionality
	As an automated tester
	I want to test different permutations and combinations of data

 
Scenario: Selecting Amenities filter returns the listing result
	Given I am on the search page refine panel with Auckland and 9 berths filter
	When I select Instant book checkbox 
	And I select Motorhome checkbox
	And I unselect Toilet and Kitchen filter
	And I select Shower filter
	And I check Auto and Manual Transmission filter
	Then I should be not see result search card

Scenario: Selecting Motorhome returns the listing
	Given I am on the search page refine panel with Auckland and 9 berths filter
	When I select Instant book checkbox 
	And I select Motorhome checkbox
	And I unselect Toilet and Shower filter
	And I select Kitchen filter
	And I check Auto and Manual Transmission filter
	Then I should be able to see result search card for Motorhome

Scenario: Selecting Campervan returns the listing
	Given I am on the search page refine panel with Auckland and 9 berths filter
	When I select Instant book checkbox
	And I select Campervan checkbox 
	And I select Toilet filter
	And I select Shower filter
	And I select Kitchen filter
	And I check Auto and Manual Transmission filter
	Then I should be able to see result search card for Campervan


Scenario: Selecting Caravan returns the listing
	Given I am on the search page refine panel with Auckland and 9 berths filter
	When I select Instant book checkbox 
	And I select Caravan checkbox
	And I select Toilet filter
	And I select Shower filter
	And I select Kitchen filter
	And I uncheck Auto and Manual Transmission filter
	Then I should be able to see result search card for Caravan

Scenario: Selecting Fifthwheeler returns the listing
	Given I am on the search page refine panel with Auckland and 9 berths filter
	When I select Instant book checkbox 
	And I select Fifthwheeler checkbox
	And I select Toilet filter
	And I select Shower filter
	And I select Kitchen filter
	And I uncheck Auto and Manual Transmission filter
	Then I should be able to see 0 results message


Scenario: Selecting Heavy Vehicle/Bus returns the listing
	Given I am on the search page refine panel with Auckland and 9 berths filter
	When I select Instant book checkbox
	And I select Heavy Vehicle checkbox 
	And I select Toilet filter
	And I select Shower filter
	And I select Kitchen filter
	Then I should be able to see result search card for Bus
