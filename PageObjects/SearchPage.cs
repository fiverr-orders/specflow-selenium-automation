using System;
using System.Collections.Generic;
using Coypu;

namespace SpecFlowAutomationSuite.PageObjects
{
    public class SearchPage : BasePage
    {
        private ElementScope FromDate { get; set; }
        private ElementScope ToDate { get; set; }
        private ElementScope CookieConsent { get; set; }
        private ElementScope ToiletCheckBox { get; set; }
        private ElementScope ShowerCheckbox { get; set; }
        private ElementScope KitchenCheckbox { get; set; }
        private ElementScope VehicleTypeInfo { get; set; }
        private ElementScope AmenitiesInfo { get; set; }
        private ElementScope TransmissionAuto { get; set; }
        private ElementScope TransmissionManual { get; set; }
        private ElementScope DateRangePicker { get; set; }
        private ElementScope CamperVan { get; set; }
        private ElementScope MotorHome { get; set; }
        private ElementScope FifthWheel { get; set; }
        private ElementScope Caravan { get; set; }
        private ElementScope Bus { get; set; }
        private ElementScope RefineButton { get; set; }
        private ElementScope Location { get; set; }
        private ElementScope BirthDropDown { get; set; }
        private Dictionary<string, string> SearchResults { get; set; }

        public SearchPage()
        {
            var xPathSearchResultCardMotorHome =
                "//div[@class='slick-slide slick-active slick-center slick-current'][@aria-hidden='false']/div/img[@alt= 'Digeiforous Gigling Machine 0']";
            var xPathSearchResultCardCamperVan =
                "//div[@class='slick-slide slick-active slick-center slick-current'][@aria-hidden='false']/div/img[@alt= 'Inst NZ - Testy McAutomation Dogervan 0']";
            var xPathSearchResultCardCaravan =
                "//div[@class='slick-slide slick-active slick-center slick-current'][@aria-hidden='false']/div/img[contains(@alt, 'Galaxdoge')]";
            var xPathSearchResultCardBus =
                "//div[@class='slick-slide slick-active slick-center slick-current'][@aria-hidden='false']/div/img[@alt='[Managed] The Dogeification Party Home 0']";

            this.SearchResults = new Dictionary<string, string>
            {
                {"Motorhome", xPathSearchResultCardMotorHome},
                {"Campervan", xPathSearchResultCardCamperVan},
                {"Caravan", xPathSearchResultCardCaravan},
                {"Bus", xPathSearchResultCardBus}
            };
        }

        public void NavigateToRental()
        {
            this.Browser.Visit(this.Settings.MighWay.RentalPagePath);
        }

        public void FindElements()
        {
            this.DateRangePicker = this.Browser.FindCss(".DateRangePickerInput__calendar-icon");
            // var logoutButton = this.Browser.FindCss("/logout");
            this.CookieConsent = this.Browser.FindId("giveCookieConsent", new Options
            {
                Timeout = TimeSpan.FromSeconds(30),
                RetryInterval = TimeSpan.FromSeconds(5)
            });

            this.Bus = this.Browser.FindId("a");
            this.CamperVan = this.Browser.FindId("b"); //input[id='b']
            this.MotorHome = this.Browser.FindId("c");
            this.Caravan = this.Browser.FindId("crv");
            this.FifthWheel = this.Browser.FindId("fw");
            this.TransmissionAuto = this.Browser.FindId("automatic");
            this.TransmissionManual = this.Browser.FindId("manual");
            this.VehicleTypeInfo = this.Browser.FindXPath("//h3[text()='Vehicle Type']/button");
            this.AmenitiesInfo = this.Browser.FindXPath("//h3[text()='Amenities']/button");
            this.ToiletCheckBox = this.Browser.FindId("toilet");
            this.KitchenCheckbox = this.Browser.FindId("shower");
            this.ShowerCheckbox = this.Browser.FindId("kitchen");
            this.Location = this.Browser.FindId("search-result-listings");
            this.BirthDropDown = this.Browser.FindId("react-select-3--value");
        }

        public string GetLocation()
        {
            return this.Location.Value;
        }

        public string GetTitle()
        {
            return this.Browser.Title;
        }

        public void ClickConsentForCookies()
        {
            this.CookieConsent.Click(new Options
                {Timeout = TimeSpan.FromSeconds(10), RetryInterval = TimeSpan.FromSeconds(1)});
        }

        public bool DateRangePickerExists()
        {
            return this.DateRangePicker.Exists();
        }

        public void SelectDateRange()
        {
            this.DateRangePicker.Click();

            this.FromDate = this.Browser.FindCss(
                ".CalendarMonth[data-visible='true'] .CalendarDay.CalendarDay--valid:nth-child(7n+1)",
                new Options {Match = Match.First}
            );

            this.ToDate = this.Browser.FindCss(
                ".CalendarMonth[data-visible='true'] .CalendarDay.CalendarDay--valid:nth-child(7n+1)",
                new Options {Match = Match.First}
            );

            //if tests fail 7n+1, click on down arrow, and then click on 7n+3 element
            this.FromDate.Click();
            this.Browser.FindCss(
                ".DayPickerNavigation__next.DayPickerNavigation__next--default"
            ).Click();
            this.ToDate.Click();
        }

        public bool RefineButtonExists()
        {
            this.RefineButton = this.Browser.FindCss(
                ".refine-panel-actions-button-span",
                new Options {Match = Match.First}
            );

            return this.RefineButton.Exists();
        }

        public void ClickRefineButton()
        {
            if (this.RefineButtonExists())
            {
                this.RefineButton.Click();
            }
        }

        public bool RefineSearchDialogExists()
        {
            return this.Browser.FindCss(".sc-jKJlTe.eZlRyL ").Exists();
        }

        public bool InstantBookingTextExists()
        {
            return this.Browser.FindXPath(
                "//p[text()='Confirm your booking instantly, there’s no need to wait for the owner to respond.']"
            ).Exists();
        }

        public bool VehicleTypeIconButtonExists()
        {
            return this.Browser.FindXPath("//h3[text()='Vehicle Type']/button").Exists();
        }

        public void ClickVehicleTypeInfo()
        {
            this.VehicleTypeInfo.Click();
        }

        public void ClickAmenitiesInfo()
        {
            this.AmenitiesInfo.Click();
        }

        public void CheckInstantBook()
        {
            this.Browser.FindId("instantBook").Check();
        }

        public bool ToiletCheckboxExists()
        {
            return this.ToiletCheckBox.Exists(new Options
            {
                Timeout = TimeSpan.FromSeconds(10),
                RetryInterval = TimeSpan.FromSeconds(1)
            });
        }

        public bool ShowerCheckboxExists()
        {
            return this.ShowerCheckbox.Exists(new Options
            {
                Timeout = TimeSpan.FromSeconds(10),
                RetryInterval = TimeSpan.FromSeconds(1)
            });
        }

        public bool KitchenCheckboxExists()
        {
            return this.KitchenCheckbox.Exists(new Options
            {
                Timeout = TimeSpan.FromSeconds(10),
                RetryInterval = TimeSpan.FromSeconds(1)
            });
        }

        public bool CamperVanExists()
        {
            return this.CamperVan.Exists(new Options
            {
                Timeout = TimeSpan.FromSeconds(10),
                RetryInterval = TimeSpan.FromSeconds(1)
            });
        }

        public bool MotorHomeExists()
        {
            return this.MotorHome.Exists(new Options
            {
                Timeout = TimeSpan.FromSeconds(10),
                RetryInterval = TimeSpan.FromSeconds(1)
            });
        }

        public bool CaravanExists()
        {
            return this.Caravan.Exists(new Options
            {
                Timeout = TimeSpan.FromSeconds(10),
                RetryInterval = TimeSpan.FromSeconds(1)
            });
        }

        public bool FifthWheelExists()
        {
            return this.FifthWheel.Exists(new Options
            {
                Timeout = TimeSpan.FromSeconds(10),
                RetryInterval = TimeSpan.FromSeconds(1)
            });
        }

        public bool BusExists()
        {
            return this.Bus.Exists(new Options
            {
                Timeout = TimeSpan.FromSeconds(10),
                RetryInterval = TimeSpan.FromSeconds(1)
            });
        }

        public void SelectAmenities()
        {
            this.ToiletCheckBox.Check();
            this.ShowerCheckbox.Check();
            this.KitchenCheckbox.Check();
        }

        public bool TransmissionAutoExists()
        {
            return
                this.TransmissionAuto
                    .Exists(); //(new Options{Timeout = TimeSpan.FromSeconds(10),RetryInterval = TimeSpan.FromSeconds(1)}));
            //_browser.ClickButton("filter-transmission");
        }

        public bool TransmissionManualExists()
        {
            return this.TransmissionManual.Exists();
        }

        public void CheckBothTransmissions()
        {
            this.TransmissionAuto.Check();
            this.TransmissionManual.Check();
        }

        public void UncheckBothTransmissions()
        {
            this.TransmissionAuto.Uncheck();
            this.TransmissionManual.Uncheck();
        }

        public void ToggleVehicleCheckboxes()
        {
            this.CamperVan.Check();
            this.MotorHome.Check();
            this.FifthWheel.Check();
            this.Caravan.Check();
            this.Bus.Check();
            this.MotorHome.Uncheck();
            this.FifthWheel.Uncheck();
            this.Caravan.Uncheck();
            this.Bus.Uncheck();
        }

        public void CheckMotorHome()
        {
            this.MotorHome.Check();
        }

        public void UncheckToilet()
        {
            this.ToiletCheckBox.Uncheck();
        }

        public void UncheckKitchen()
        {
            this.KitchenCheckbox.Uncheck();
        }

        public void CheckShower()
        {
            this.ShowerCheckbox.Check();
        }

        public void VerifyInstantBook()
        {
            // _browser.FindId("filter-instantbooking").Click();
            // Assert.IsTrue(_browser.FindCss("label.checkbox-label[for='box1']").Exists());
            // _browser.FindCss("label.checkbox-label[for='box1']").Click();
            // include code for verifying instant book badge on listing img.
        }

        public void VerifyFilterResults()
        {
            // verify the number of results when toilet,kitchen and shower filters are applied
            // Manual filter has only 1 result Doggered listing
        }

        public void VerifyFeatureBadge()
        {
            // verify featured badge on search card
        }

        public void VerifyListingCardPrice()
        {
            //verify both seasonal rates and off-peak rates
            this.Browser.FindCss("label.checkbox-label[for='box1']");
        }

        public void VerifyListingCardDetails()
        {
            //verify listing's rego,location and vehicle type,amenities on search card
        }

        public void VerifyCardMaps()
        {
            //verify map mini cards and geo tags
            //click on geotag displays mini card and clicking off collapses card
        }

        public void SelectBirths(int birthCount)
        {
            this.BirthDropDown.Click();
            this.Browser.FindId("react-select-3--list").Exists();
            this.Browser.FindId($"react-select-3--option-{birthCount - 1}").Click();
        }

        public string GetSelectedBirths()
        {
            return this.Browser.FindId("react-select-3--value-item").Text;
        }

        public void UncheckShower()
        {
            this.ShowerCheckbox.Uncheck();
        }

        public void CheckCamperVan()
        {
            this.CamperVan.Check();
        }

        public void CheckCaravan()
        {
            this.Caravan.Check();
        }

        public void CheckFifthWheeler()
        {
            this.FifthWheel.Check();
        }

        public void CheckBus()
        {
            this.Bus.Check();
        }

        public bool LocationMessageExists()
        {
            return this.Browser.FindXPath("//span[text()='- Check the spelling of your pick-up location']").Exists();
        }

        public bool FilterMessageExists()
        {
            return this.Browser.FindXPath("//span[text()='- Remove some of your filters']").Exists();
        }

        public bool TravelDatesMessageExists()
        {
            return this.Browser.FindXPath("//span[text()='- Amend your travel dates']").Exists();
        }

        public bool ResultSearchCardExists(string cardName)
        {
            return this.Browser
                .FindXPath(this.SearchResults.GetValueOrDefault(cardName))
                .Exists();
        }

        public bool ResultSearchCardDoesNotExist()
        {
            return this.Browser.FindXPath("//div[text()='DOGEBOT-9201']").Exists();
        }
    }
}