using System;
using Coypu;
using SpecFlowAutomationSuite.Helpers;

namespace SpecFlowAutomationSuite.PageObjects
{
    public class BasePage: IDisposable
    {
        protected BrowserSession Browser { get; private set; }

        protected readonly TestSettings Settings;

        protected BasePage()
        {
            this.Settings = new TestSettings();
            this.Browser = BrowserFactory.GetBrowser(this.Settings);
        }

        public void Dispose()
        {
            if (this.Browser == null)
                return;
            
            this.Browser.Dispose();
            this.Browser = null;
        }
    }
}